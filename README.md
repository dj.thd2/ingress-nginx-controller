# Ingress NGINX Controller

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5691/badge)](https://bestpractices.coreinfrastructure.org/projects/5691)
[![Go Report Card](https://goreportcard.com/badge/github.com/kubernetes/ingress-nginx)](https://goreportcard.com/report/github.com/kubernetes/ingress-nginx)
[![GitHub license](https://img.shields.io/github/license/kubernetes/ingress-nginx.svg)](https://github.com/kubernetes/ingress-nginx/blob/main/LICENSE)
[![GitHub stars](https://img.shields.io/github/stars/kubernetes/ingress-nginx.svg)](https://github.com/kubernetes/ingress-nginx/stargazers)
[![GitHub stars](https://img.shields.io/badge/contributions-welcome-orange.svg)](https://github.com/kubernetes/ingress-nginx/blob/main/CONTRIBUTING.md)


## Overview

ingress-nginx is an Ingress controller for Kubernetes using [NGINX](https://www.nginx.org/) as a reverse proxy and load
balancer.

[Learn more about Ingress on the main Kubernetes documentation site](https://kubernetes.io/docs/concepts/services-networking/ingress/).

## Motivation and differences with official version

The target of this fork is to enable running NGINX Ingress Controller side-by-side with HAProxy on the same pod, letting NGINX to
serve plain HTTP traffic and route requests to the appropriate backend, but using HAProxy as a TLS termination proxy to serve
HTTPS traffic and then forwarding the requests to NGINX on HTTP. This aims to balance the greater power and customization capabilities
of NGINX in order to route, filter and cache HTTP requests, with the higher performance of HAProxy in terms of TLS connection handling in
high traffic/concurrency environments.

To accomplish it, it has been decided to use HAProxy as a frontend to expose the TLS public port with a simple configuration which only
forwards all the incoming requests to port 80 of the NGINX container, using PROXY Protocol in order to keep the original client IP in
NGINX. The TLS certificates will be read from `/etc/ingress-controller/ssl`, which gets populated by NGINX Ingress Controller with certificate
data got from K8s TLS secrets from ingresses.

This version has been modified to always store all the TLS certificates and keys to files in disk, where the original version only
stores the default selfsigned certificate and the certificates which have a embedded CA certificate. This caused, when used together with
cert-manager and in many other cases, for example, Lets Encrypt issued certificates, which do not embed the CA certificate into the
Kubernetes TLS secret, won't get written to disk and only kept in memory by the Ingress Controller to be later loaded into Nginx by
LUA scripts. By storing always all the TLS certificate data, we ensure the sidecar HAProxy can read all the certificate info and select
the correct certificate respecting the ingress settings and keeping all the system in sync with K8s resource definitions as expected.

Additionally, as the original project was on Github instead of Gitlab, a basic .gitlab-ci.yml config file has been created in order to
build and deploy the image to the Gitlab container registry.

## Get started

See the [Getting Started](https://kubernetes.github.io/ingress-nginx/deploy/) document.

## Troubleshooting

If you encounter issues, review the [troubleshooting docs](docs/troubleshooting.md),
[file an issue](https://github.com/kubernetes/ingress-nginx/issues), or talk to us on the
[#ingress-nginx channel](https://kubernetes.slack.com/messages/ingress-nginx) on the Kubernetes Slack server.

## Changelog

See [the list of releases](https://github.com/kubernetes/ingress-nginx/releases) to find out about feature changes.
For detailed changes for each release; please check the [Changelog.md](Changelog.md) file.
For detailed changes on the `ingress-nginx` helm chart, please check the following
[CHANGELOG.md](charts/ingress-nginx/CHANGELOG.md) file.

### Supported Versions table

Supported versions for the ingress-nginx project mean that we have completed E2E tests, and they are passing for
the versions listed. Ingress-Nginx versions may work on older versions but the project does not make that guarantee.

|    | Ingress-NGINX version | k8s supported version        | Alpine Version | Nginx Version | Helm Chart Version |
|:--:|-----------------------|------------------------------|----------------|---------------|--------------------|
| 🔄 | **v1.8.1**            | 1.27,1.26, 1.25, 1.24        | 3.18.2         | 1.21.6        | 4.7.*              |
| 🔄 | **v1.8.0**            | 1.27,1.26, 1.25, 1.24        | 3.18.0         | 1.21.6        | 4.7.*              |
| 🔄 | **v1.7.1**            | 1.27,1.26, 1.25, 1.24        | 3.17.2         | 1.21.6        | 4.6.*              |
| 🔄 | **v1.7.0**            | 1.26, 1.25, 1.24             | 3.17.2         | 1.21.6        | 4.6.*              |
| 🔄 | **v1.6.4**            | 1.26, 1.25, 1.24, 1.23       | 3.17.0         | 1.21.6        | 4.5.*              |
|    | v1.5.1                | 1.25, 1.24, 1.23             | 3.16.2         | 1.21.6        | 4.4.*              |
|    | v1.4.0                | 1.25, 1.24, 1.23, 1.22       | 3.16.2         | 1.19.10†      | 4.3.0              |
|    | v1.3.1                | 1.24, 1.23, 1.22, 1.21, 1.20 | 3.16.2         | 1.19.10†      | 4.2.5              |
|    | v1.3.0                | 1.24, 1.23, 1.22, 1.21, 1.20 | 3.16.0         | 1.19.10†      | 4.2.3              |
|    | v1.2.1                | 1.23, 1.22, 1.21, 1.20, 1.19 | 3.14.6         | 1.19.10†      | 4.1.4              |
|    | v1.1.3                | 1.23, 1.22, 1.21, 1.20, 1.19 | 3.14.4         | 1.19.10†      | 4.0.19             |
|    | v1.1.2                | 1.23, 1.22, 1.21, 1.20, 1.19 | 3.14.2         | 1.19.9†       | 4.0.18             |
|    | v1.1.1                | 1.23, 1.22, 1.21, 1.20, 1.19 | 3.14.2         | 1.19.9†       | 4.0.17             |
|    | v1.1.0                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.13             |
|    | v1.0.5                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.9              |
|    | v1.0.4                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.6              |
|    | v1.0.3                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.5              |
|    | v1.0.2                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.3              |
|    | v1.0.1                | 1.22, 1.21, 1.20, 1.19       | 3.14.2         | 1.19.9†       | 4.0.2              |
|    | v1.0.0                | 1.22, 1.21, 1.20, 1.19       | 3.13.5         | 1.20.1        | 4.0.1              |


† _This build is
[patched against CVE-2021-23017](https://github.com/openresty/openresty/commit/4b5ec7edd78616f544abc194308e0cf4b788725b#diff-42ef841dc27fe0b5aa2d06bd31308bb63a59cdcddcbcddd917248349d22020a3)._

See [this article](https://kubernetes.io/blog/2021/07/26/update-with-ingress-nginx/) if you want upgrade to the stable
Ingress API.

## Get Involved

Thanks for taking the time to join our community and start contributing!

- This project adheres to the [Kubernetes Community Code of Conduct](https://git.k8s.io/community/code-of-conduct.md).
  By participating in this project, you agree to abide by its terms.

- **Contributing**: Contributions of all kind are welcome!

  - Read [`CONTRIBUTING.md`](CONTRIBUTING.md) for information about setting up your environment, the workflow that we
    expect, and instructions on the developer certificate of origin that we require.
  - Join our Kubernetes Slack channel for developer discussion : [#ingress-nginx-dev](https://kubernetes.slack.com/archives/C021E147ZA4).
  - Submit GitHub issues for any feature enhancements, bugs or documentation problems.
    - Please make sure to read the [Issue Reporting Checklist](https://github.com/kubernetes/ingress-nginx/blob/main/CONTRIBUTING.md#issue-reporting-guidelines) before opening an issue. Issues not conforming to the guidelines **may be closed immediately**.
  - Join our [ingress-nginx-dev mailing list](https://groups.google.com/a/kubernetes.io/g/ingress-nginx-dev/c/ebbBMo-zX-w)

- **Support**:
  - Join the [#ingress-nginx-users](https://kubernetes.slack.com/messages/CANQGM8BA/) channel inside the [Kubernetes Slack](http://slack.kubernetes.io/) to ask questions or get support from the maintainers and other users.
  - The [GitHub issues](https://github.com/kubernetes/ingress-nginx/issues) in the repository are **exclusively** for bug reports and feature requests.
  - **Discuss**: Tweet using the `#IngressNginx` hashtag or sharing with us [@IngressNginx](https://twitter.com/IngressNGINX).

## License

[Apache License 2.0](https://github.com/kubernetes/ingress-nginx/blob/main/LICENSE)
